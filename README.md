xHotels - Limehome Coding Challange
-------------------------------------
View the deployed version [here](https://xhotels.paas.eneepo.com).

### Features that I worked on:
#### Mini CSS Framework
Created a 12 columnd grid system with some styles using SASS for the neccessary elements. You can read the codes here: `xhotels/core/static/core/scss/framework`. Sample code using the grid system:

```html
<div class="grid container">
    <div class="row">
        <div class="four col">A four-column width</div>
        <div class="eight col">An eight-column width</div>
    </div>
</div>
```
#### Custom django management command to import hotels' data
You can import the hotels data around any location using the following command `./manage.py hotels "48.135442,11.543352" -s 2000`.
The command uses the Here Api to fetch the data. The `-s` switch can be used for the size of data(Total number of hotels to be imported.)

#### REST API + Documentation
You can use the API to create/put/delete/patch,get hotels and exploring the hotels near a point

#### Deployment 
Created a SAAS server and deployed the web application, also used Open Stack Swift for the storage instead of using Amazon S3.

#### Easy development using gulp
With a single `gulp` command you can run the django webserver, build the stack and syncing the changes to the browser in realtime. Also, all the of logs(dev server, sass, build, proxy, ...) at same place.

#### Vanilla js for the Front end
The only library that I used was lodash(For templating), other that I used plain javaScript for everthing.

### Features that I should worked on:
* Add more tests: 
	* Explore endpoint doesn't have any test
	* Some functional test for the explore page
	* For the hotel management command
* Making it SPA using vue.js or react
* Making the grid responsive
* Using .env for keeping environment variables and removing the hard coded api keys

Instructions
-------------------------------------

### Development
Assuming you have created a virtual environment and have installed docker, use the following instrucitons:
The easiest way to run PostgreSQl with GIS extetions would be using docker.
### Running the server
1. Create a postgres with gis container using `mdillon/postgis`
```sh
docker run --name xhotels_postgis_db -p 5432:5432 -e POSTGRES_PASSWORD=pass -e POSTGRES_USER=user -e POSTGRES_DB=db -d mdillon/postgis:9.6
```
2. Go to xhotels directory and run the server
```sh
cd xhotels/
./manage.py migrate
# importing sample data near a geopoint for hotels using Here API
./manage.py hotels "48.135442,11.543352" -s 2000
./manage.py runserver
```
### Asset building/serving
Assuming you have npm and gulp-cli installed:
1. Install the packages
```sh
npm install
```
2. Make sure you are in the correct virtual env and run the `gulp` command. It runs the default task which is configured to build the assets, runs the django server, and lastly runs the browser sync.
```sh
gulp
```
*If the browser opened and showed nothing, please open a new tap and got to locahost:3000*
```sh
# Building the js files
gulp js
# Building the css from scss files 
gulp styles
# Serving the django development server
gulp django
```

### Testing
There is a management command in core application which runs the tests and coverage at the same time. Just simply run the django tests.
1. Go to xhotels directory
2. Run `./manage.py test`

### Deployment
Application has been configured for deployment on [CapRover](https://caprover.com/) and [Swift(OpenStack object storage)](https://wiki.openstack.org/wiki/Swift). CapRover is a self-hosting SAAS and its [setup](https://caprover.com/docs/get-started.html) on a bare server takes less than 10 minutes.

#### 1) Assumptions
The following assumptions are made about your environment:
- You have a working CapRover
- You have read the basic CapRover documentation
- You have ssh access to you CapRover server

### 2) Preperation: CapRover
Preparing the deployment environment
#### Database
1. Create a new blank app with the name `xhotels-postgis-db` with persistant data enabled.
2. Fill the environment variables below and enter them with the bulk edit mode and save the settings
    ```
    POSTGRES_USER=FILL_IT_PLEASE
    POSTGRES_PASSWORD=FILL_IT_PLEASE
    POSTGRES_DB=FILL_IT_PLEASE
    ```
2. Go to the deployment tab at the bottom of the page fill the `Method 5: Deploy captain-definition file` text area woth following configuration and press `Deploy Now`:
    ```
    {
        "schemaVersion": 2,
        "imageName": "mdillon/postgis:9.6"
    }
    ```
#### App
1. Create a new blank app with the name `xhotels`
2. Fill the environment variables below and enter them with the bulk edit mode
    ```
    CAPROVER=True
    CR_SECRET_KEY=<YOUR SECRET KEY>
    CR_HOSTS=xhotels.<YOUR_CAPROVER_DOMAIN>.com
    CR_DB_NAME=FILL_IT_PLEASE
    CR_DB_USER=FILL_IT_PLEASE
    CR_DB_PASSWORD=FILL_IT_PLEASE
    CR_DB_HOST=srv-captain--xhotels-postgres-db
    CR_DB_PORT=5432
    SWIFT_USERNAME=FILL_IT_PLEASE
    SWIFT_KEY=FILL_IT_PLEASE
    SWIFT_PROJECT_NAME=FILL_IT_PLEASE
    SWIFT_PROJECT_ID=FILL_IT_PLEASE
    SWIFT_USER_DOMAIN_NAME=FILL_IT_PLEASE
    SWIFT_REGION_NAME=FILL_IT_PLEASE
    SWIFT_PROJECT_DOMAIN_ID=FILL_IT_PLEASE
    STATIC_URL=FILL_IT_PLEASE
    ```
### 3) Django
There are [multiple ways](https://caprover.com/docs/deployment-methods.html) to deploy your app to CapRover. But the easiest would be using command line.
1. cd to the root of this repo
2. Use this command to deploy `caprover deploy`
3. Follow the instructions and use the `xhotels` app that you built.

