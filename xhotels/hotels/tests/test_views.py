import json
from django.test import TestCase, Client
from django.urls import reverse

from rest_framework import status

from ..models import Hotel, HotelFeature
from ..serializers import HotelSerializer


# initialize the APIClient app
client = Client()

class GetAllHotelsTest(TestCase):
    """ Test suite for the hotels API """

    def setUp(self):
        self.plaza = Hotel.objects.create(
            name='The Plaza', 
            description='Opened in 1907 and designated an official landmark in 1969.', 
            hotel_class=5,
            point= 'POINT(14.679667 120.541293)'
        )
        self.ritz = Hotel.objects.create(
            name='Hotel Ritz',
            description='Famously the headquarters of Coco Chanel, Ernest Hemingway and Ingrid Bergman.',
            hotel_class=4,
            point= 'POINT(14.679667 120.541293)'
        )
        self.raffles = Hotel.objects.create(
            name='Raffles', 
            description='Named after Stamford Raffles, the founder of Singapore.',
            hotel_class=3,
            point='POINT(14.679667 120.541293)'
        )

    def test_get_all_hotels(self):
        response = client.get(reverse('hotel-list'))
        hotels = Hotel.objects.all()
        serializer = HotelSerializer(hotels, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_valid_single_hotel(self):
        response = client.get(
            reverse('hotel-detail', kwargs={'pk': self.plaza.pk})
        )
        hotel = Hotel.objects.get(pk=self.plaza.pk)
        serializer = HotelSerializer(hotel)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_hotel(self):
        response = client.get(
            reverse('hotel-detail', kwargs={'pk': 9999}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewHotelTest(TestCase):
    """ Test module for inserting a new hotel """

    def setUp(self):
        self.feature_parking = HotelFeature.objects.create(
            name='The Parking'
        )
        self.valid_payload = {
            'name': 'The Plaza', 
            'description': 'Opened in 1907 and designated an official landmark in 1969.', 
            'features': [self.feature_parking.pk],
            'hotel_class': 5,
            'point': {
                'type': 'Point',
                'coordinates': [-123.0208, 44.0464],
            }
        }
        self.valid_payload_without_hotel_class = {
            'name': 'The Plaza', 
            'description': 'Opened in 1907 and designated an official landmark in 1969.', 
            'point': {
                'type': 'Point',
                'coordinates': [-123.0208, 44.0464],
            }
        }
        self.invalid_payload = {
            'name': '',
            'description': 'Famously the headquarters of Coco Chanel, Ernest Hemingway and Ingrid Bergman.',
            'hotel_class': 6,
        }

    def test_create_valid_hotel(self):
        response = client.post(
            reverse('hotel-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_valid_hotel_without_hotel_class(self):
        response = client.post(
            reverse('hotel-list'),
            data=json.dumps(self.valid_payload_without_hotel_class),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_hotel(self):
        response = client.post(
            reverse('hotel-list'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateSingleHotelTest(TestCase):
    """ Test module for updating an existing hotel record """

    def setUp(self):
        self.plaza = Hotel.objects.create(
            name='Plaza', 
            description='Opened in 1907 and designated an official landmark in 1969.', 
            hotel_class=1,
            point= 'POINT(14.679667 120.541293)'
        )
        self.valid_payload = {
            'name': 'The Plaza',
            'description': 'Opened in 1907 and designated an official landmark in 1969.', 
            'hotel_class': 5,
            'point': {
                'type': 'Point',
                'coordinates': [-123.0208, 44.0464],
            }
        }
        self.invalid_payload = {
            'name': '',
            'description': '',
            'hotel_class': 6,
        }

    def test_valid_update_hotel(self):
        response = client.put(
            reverse('hotel-detail', kwargs={'pk': self.plaza.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_hotel(self):
        response = client.put(
            reverse('hotel-detail', kwargs={'pk': self.plaza.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSingleHotelTest(TestCase):
    """ Test module for deleting an existing hotel record """

    def setUp(self):
        self.plaza = Hotel.objects.create(
            name='The Plaza', 
            description='Opened in 1907 and designated an official landmark in 1969.', 
            hotel_class=5,
            point= 'POINT(14.679667 120.541293)'
        )
        self.ritz = Hotel.objects.create(
            name='Hotel Ritz',
            description='Famously the headquarters of Coco Chanel, Ernest Hemingway and Ingrid Bergman.',
            hotel_class=4,
            point= 'POINT(14.679667 120.541293)'
        )

    def test_valid_delete_hotel(self):
        response = client.delete(
            reverse('hotel-detail', kwargs={'pk': self.plaza.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_hotel(self):
        response = client.delete(
            reverse('hotel-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)