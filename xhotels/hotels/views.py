from decimal import Decimal

from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.measure import D
from django.shortcuts import HttpResponse
from django.views.generic.base import TemplateView

from rest_framework import viewsets
from rest_framework import status
from rest_framework import mixins
from rest_framework.decorators import api_view
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from .models import Hotel, HotelFeature
from .serializers import HotelSerializer, HotelFeatureSerializer


class ExplorePageView(TemplateView):
    template_name = "hotels/explore.html"


class HotelFeatureViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Fetures of the hotel like parking and pool can be accessed via this Endpoint
    """
    queryset = HotelFeature.objects.all()
    serializer_class = HotelFeatureSerializer


class HotelViewSet(viewsets.ModelViewSet):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer


class ExploreViewSet(mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    """
    Explore endpoint will display hotels near a geopoint neighborhood,
    """
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                'at',
                openapi.IN_QUERY,
                description='Coordinates of search location expressed as latitude, longitude.',
                type=openapi.TYPE_STRING,
                default='48.135442,11.543352'
            )
        ]
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def get_queryset(self):
        """
        Filtering the hotels closer to the defined point and oredering them by distance
        """
        at = self.request.query_params.get('at', None)
        try:
            point = Point([Decimal(coordinate.strip())
                           for coordinate in at.split(',')])
        except Exception:
            point = Point((5, 23))
        radius = 5000

        queryset = Hotel.objects.filter(point__distance_lt=(point, D(m=radius)))\
            .annotate(distance=Distance('point', point))\
            .order_by('distance')

        return queryset
