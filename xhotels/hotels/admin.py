from django.contrib import admin

from .models import Hotel, HotelFeature


@admin.register(HotelFeature)
class HotelFeatureModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Hotel)
class HotelModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'vicinity', 'average_rating')