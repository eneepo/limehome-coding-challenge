from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from .models import Hotel, HotelFeature


class HotelFeatureSerializer(serializers.ModelSerializer):

    class Meta:
        model = HotelFeature
        fields = ['pk', 'name',]



class HotelSerializer(serializers.ModelSerializer):
    features = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = Hotel
        geo_field = 'point'
        fields = '__all__'
