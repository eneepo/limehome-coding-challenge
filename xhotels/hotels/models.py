from django.contrib.gis.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import gettext_lazy as _


class HotelFeature(models.Model):
    """
    Hotel features(like having a parking lot or a bar) are stored in this model.

    TODO: Add categorization
    """
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=128
    )

    def __str__(self):
        return self.name


class Hotel(models.Model):
    """ 
    Main model that keeps hotels data

    TODO: Model I18n for the name and description fields
    """
    name = models.CharField(
        verbose_name=_('Name'),
        max_length=128
    )
    description = models.TextField(
        verbose_name=_('Description'),
        null=True,
        blank=True
    )
    hotel_class = models.PositiveSmallIntegerField(
        verbose_name=_('Hotel class'),
        default=0,
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)
        ]
    )
    average_rating = models.DecimalField(
        verbose_name=_('Average rating'),
        max_digits=3, 
        decimal_places=2,
        default=0,
        validators=[
            MaxValueValidator(5),
            MinValueValidator(0)
        ]
    )
    features = models.ManyToManyField(
        HotelFeature,
        verbose_name=_('Features'),
        blank=True
    )
    point = models.PointField(
        verbose_name=_('Location'),
        srid=4326,
    )    
    vicinity = models.TextField(
        verbose_name=_('Vicinity'),
        null=True,
        blank=True
    )    
    created_at = models.DateTimeField(
        verbose_name=_('Create date'),
        auto_now_add=True
    )
    updated_at = models.DateTimeField(
        verbose_name=_('Update date'), 
        auto_now=True
    )

    def __str__(self):
        return self.name
