import json
from itertools import islice
import requests
from typing import Dict
from urllib import parse

from django.apps import apps
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.contrib.gis.geos import Point 

from hotels.models import Hotel


class Command(BaseCommand):
    help = "Import hotels from HERE API"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_url = 'https://places.sit.ls.hereapi.com/places/v1/'
        self.api_key = getattr(settings, "HERE_API_KEY", 'uLFKJPcmKETgXkOBm5qWRyIQ5nM4kZgBrDTLo59_l5U')
        self.category = 'hotel'
        self.size = 20

    def insert_hotels_to_db(self, data:Dict) -> None:

        objs = []
        for item in data['results']['items']:
            hotel = Hotel(
                name=item['title'], 
                vicinity=item['vicinity'], 
                average_rating=item['averageRating'],
                point= Point(item['position'])
            )
            objs.append(hotel)

        Hotel.objects.bulk_create(objs)
        

    def get_endpoint(self, endpoint):
        return parse.urljoin(self.base_url, endpoint)
    
    def browse_hotels_here(self, point: str) -> Dict:
        params = {
            'at': point,
            'cat': self.category,
            'size': self.size,
            'apikey': self.api_key
        }
        url = self.get_endpoint('browse')

        try:
            response = requests.get(url, params)
            response.raise_for_status()
        except Exception as err:
            print(f'Ann error occurred: {err}')
        else:
            return response.json()

    def add_arguments(self, parser):
            parser.add_argument(
                'geopoint',
                type=str,
                help='Location that you want to import nearby hotels'
            )
            
            # Optional argument
            parser.add_argument('-s', '--size', type=int, help='The maximum number of result items to be imported.', )

    def handle(self, *args, **kwargs):
        geopoint = kwargs['geopoint']

        if kwargs['size']:
            self.size = kwargs['size']

        result = self.browse_hotels_here(geopoint)
        self.insert_hotels_to_db(result)
        
        self.stdout.write('Done')