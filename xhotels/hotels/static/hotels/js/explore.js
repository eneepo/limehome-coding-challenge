document.addEventListener('DOMContentLoaded', function () {

    var formEl = document.getElementById("form"),
        locEl = document.getElementById("location"),
        mapEl = document.getElementById("map"),
        resultsEl = document.getElementById("results"),
        results = document.getElementById("results"),
        geocodingParams;

    // =========================================================
    // Creating the map ========================================
    // =========================================================

    // Initialize the platform object:
    var platform = new H.service.Platform({
        'apikey': '2P_BGznRF3899u4U-f5gWAJQaYI36VAcRz_VVHccR5k'
    });

    // Get the default map types from the Platform object:
    var defaultLayers = platform.createDefaultLayers();

    // Get the geocoder
    var geocoder = platform.getGeocodingService();

    // Instantiate the map:
    var map = new H.Map(
        mapEl,
        defaultLayers.vector.normal.map,
        {
            zoom: 10,
            center: { lng: 13.4, lat: 52.51 }
        });

    // Enabling the event system on the map instance:
    var mapEvents = new H.mapevents.MapEvents(map);
    var behavior = new H.mapevents.Behavior(mapEvents);

    // Enabling the UI zoom, scalebar and map settings
    var ui = H.ui.UI.createDefault(map, defaultLayers);
    var mapSettings = ui.getControl('mapsettings');
    var zoom = ui.getControl('zoom');
    var scalebar = ui.getControl('scalebar');

    mapSettings.setAlignment('top-left');
    zoom.setAlignment('top-left');
    scalebar.setAlignment('bottom-right');

    // =========================================================
    // Helper functions ========================================
    // =========================================================
    
    // For centering the map to a position and setting the zoom level
    function moveMapTo(map, position, zoom) {
        zoom = typeof zoom !== 'undefined' ? zoom : 14;
        map.setCenter({ lat: position[0], lng: position[1] });
        map.setZoom(zoom);
    }

    // To add info bubble 
    function addBubble() {
        var bubble = new H.ui.InfoBubble(
            event.target.getGeometry(), {
            content: '<p>' + data['a']['data']['name'] + '</p>'
        }
        );
        ui.getBubbles().forEach(bub => ui.removeBubble(bub));
        ui.addBubble(bubble);
    }

    // Generates html codes to for the hotels and adding it to the #results element
    function createItems(data, map) {
        results.innerHTML = '';
        data.forEach(function (value, index, array) {
            var template = _.template('<div class="item"><img src="https://storage.bhs.cloud.ovh.net/v1/AUTH_d4d5c49c674e483fb6de48d44b7808a0/django-static/hotels/svg/marker.svg" alt="Marker icon"><div class="content"><h3 class="title" data-point="<%= point %>"><%= name %></h3><p class="description"><%= vicinity %></p></div></div>');
            value['point'] = value['point']['coordinates'].toString();
            console.log(value['point']);
            results.innerHTML += template(value);
        });

        titles = document.getElementsByClassName("title");
        for (var i = 0; i < titles.length; i++) {
            var title = titles[i];
            title.onclick = function () {
                moveMapTo(map, this.dataset.point.split(','), 20);
            }
        }

    }

    // Creating the point clusters based on data points
    var createClusters = function (data, map) {
        var dataPoints = [];
        data.forEach(function (value, index, array) {
            var point = value['point']['coordinates'];
            dataPoints.push(new H.clustering.DataPoint(point[0], point[1], undefined, value));
        });
        var clusteredDataProvider = new H.clustering.Provider(dataPoints);

        // Create a layer that includes the data provider and its data points: 
        var layer = new H.map.layer.ObjectLayer(clusteredDataProvider);

        // Add the layer to the map:
        map.addLayer(layer);

        // Add an event listener to the Provider - this listener is called when a maker
        // has been tapped:
        clusteredDataProvider.addEventListener('tap', function (event) {
            // Log data bound to the marker that has been tapped:
            var data = event.target.getData();

            // A simple way to avoid bubbles to appear on cluster groups
            // because data is undefined on cluster groups
            if (typeof (data['a']['data']) != "undefined") {
                var bubble = new H.ui.InfoBubble(
                    event.target.getGeometry(), {
                    content: '<p>' + data['a']['data']['name'] + '</p>'
                });
                ui.getBubbles().forEach(bub => ui.removeBubble(bub));
                ui.addBubble(bubble);
            }
        });

    }


    // Getting hotels from explore end point of the REST API
    var getHotels = function (position) {
        var positionStr = ''.concat(position['Latitude'], ',', position['Longitude'])

        var xhr = new XMLHttpRequest();
        xhr.open("GET", '/api/v1/explore/?at=' + positionStr, true);
        xhr.onload = function () {
            var jsonResponse = JSON.parse(xhr.responseText);
            createClusters(jsonResponse, map)
            createItems(jsonResponse, map)
        };
        xhr.send(null);
    };

    // On successful geocode request this function will be executed
    var onResult = function (result) {
        var position = result.Response.View[0].Result[0]['Location']['DisplayPosition'];
        moveMapTo(map, [position['Latitude'], position['Longitude']]);
        getHotels(position);
    };

    // =========================================================
    // Form execution   ========================================
    // =========================================================

    // Prevent form submission
    formEl.addEventListener("submit", function (evt) {
        evt.preventDefault();
        geocodingParams = {
            searchText: locEl.value
        };
        geocoder.geocode(geocodingParams, onResult, function (e) {
            console.error(e);
        });
    });

    // Resizing the map and results area absed on bwroser height
    mapEl.style.height = ''.concat(window.innerHeight - 66, 'px');
    resultsEl.style.height = ''.concat(window.innerHeight - 350, 'px');
    map.getViewPort().resize();

}, false);