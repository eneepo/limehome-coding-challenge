const { src, dest, watch, series, parallel } = require('gulp');
const spawn = require('child_process').spawn;
const cp = require("child_process");
const gulp = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("autoprefixer");
const concat = require("gulp-concat");
const postcss = require("gulp-postcss");
const sourcemaps = require("gulp-sourcemaps");
const browserSync = require("browser-sync").create();

const files = {
  'scss': [
    './core/static/core/scss/**/*.scss'
  ],
  'html': [
    './**/templates/**/*.html',
    './templates/**/*.html'
  ],
  'js':[
    './core/static/core/js/main.js'
  ]
}

// Run the Django development server
function djangoTask(){
  return spawn("python", ["manage.py", "runserver"]).stderr.on(
    "data",
    data => {
      console.log(`${data}`);
    }
  );
}

function stylesTask() {
  return src(files.scss)
    .pipe(sass().on("error", sass.logError))
    .pipe(concat("style.css"))
    .pipe(sourcemaps.init())
    .pipe(postcss([autoprefixer()]))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("./core/static/core/css/"))
    .pipe(browserSync.stream());
}

function jsTask(){
  return
}

// Initiate browsersync and point it at localhost:8000
function brosersyncTask() {
  browserSync.init({
    notify: true,
    proxy: "localhost:8000",
  });
}

function reloadTask(done) {
  browserSync.reload();
  done();
}

function watchTask() {
  watch(files.scss, stylesTask);
  watch(
    [].concat(
      files.scss,
      files.js,
      files.html
    ),
    reloadTask);
}

exports.django = djangoTask;
exports.styles = stylesTask;
exports.js = jsTask;
exports.sync = brosersyncTask;
exports.watch = watchTask;

exports.default = parallel(watchTask, djangoTask, brosersyncTask);
