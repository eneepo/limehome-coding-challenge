"""Caprover specific django settings
Quick-start development settings - unsuitable for production
See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/
"""
 
import os

from django.core.exceptions import ImproperlyConfigured

from .settings import BASE_DIR

# key and debugging settings should not changed without care
SECRET_KEY = os.environ.get(
    "CR_SECRET_KEY") or ImproperlyConfigured("CR_SECRET_KEY not set")
DEBUG = False

# allowed hosts get parsed from a comma-separated list
hosts = os.environ.get("CR_HOSTS") or ImproperlyConfigured("CR_HOSTS not set")
try:
    ALLOWED_HOSTS = hosts.split(",")
except:
    raise ImproperlyConfigured("CR_HOSTS could not be parsed")

# Database
if os.environ.get("CR_USESQLITE"):
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
        }
    }
else:
    name = os.environ.get("CR_DB_NAME") or ImproperlyConfigured(
        "CR_DB_NAME not set")
    user = os.environ.get("CR_DB_USER") or ImproperlyConfigured(
        "CR_DB_USER not set")
    password = os.environ.get("CR_DB_PASSWORD") or ImproperlyConfigured(
        "CR_DB_PASSWORD not set")
    host = os.environ.get("CR_DB_HOST") or ImproperlyConfigured(
        "CR_DB_HOST not set")
    port = os.environ.get("CR_DB_PORT") or ImproperlyConfigured(
        "CR_DB_PORT not set")

    DATABASES = {
        "default": {
            'ENGINE': 'django.contrib.gis.db.backends.postgis',
            "NAME": name,
            "USER": user,
            "PASSWORD": password,
            "HOST": host,
            "PORT": port,
        }
    }



DEFAULT_FILE_STORAGE = 'swift.storage.SwiftStorage'
STATICFILES_STORAGE = 'swift.storage.StaticSwiftStorage'

SWIFT_CONTAINER_NAME = 'django'
SWIFT_STATIC_CONTAINER_NAME = 'django-static'

SWIFT_AUTH_URL = 'https://auth.cloud.ovh.net/v3'
SWIFT_AUTH_VERSION = '3'
SWIFT_USERNAME = os.environ.get("SWIFT_USERNAME")
SWIFT_KEY = os.environ.get("SWIFT_KEY")
SWIFT_PROJECT_NAME = os.environ.get("SWIFT_PROJECT_NAME")
SWIFT_PROJECT_ID = os.environ.get("SWIFT_PROJECT_ID")
SWIFT_REGION_NAME = os.environ.get("SWIFT_REGION_NAME")
SWIFT_USER_DOMAIN_NAME = os.environ.get("SWIFT_USER_DOMAIN_NAME")
SWIFT_PROJECT_DOMAIN_ID = os.environ.get("SWIFT_PROJECT_DOMAIN_ID")
